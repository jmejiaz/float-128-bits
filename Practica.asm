.data
op1:  .word 0x00000000 0x00000000 0x00000000 0xffff0000
op2:  .word 0x00009900 0x07000000 0x00000000 0xf9760000 
resul: .space 512

.text
main:
	la $a0,op1
	la $a1,op2
	la $a2,resul
	jal suma
#	jal bonus
	li $v0 10
	syscall

suma:
	
	add $sp,$sp,-36
	sw $s0,32($sp)
	sw $s1,28($sp)
	sw $s2,24($sp)
	sw $s3,20($sp)
	sw $s4,16($sp)
	sw $s5,12($sp)
	sw $s6,8($sp)
	sw $s7,4($sp)
	sw $ra 0($sp)

        jal cargarSignos
	move $s0, $v0         #SINGO PRIMER NUMERO
	move $s1, $v1		#SIGNO SEGUNDO NUMERO
	
	jal cargarExponentes
	move $s2, $v0         #EXPONENTE PRIMER NUMERO
	move $s3, $v1		#EXPONENTE SEGUNDO NUMERO
	
	jal mirarExponentePrimero
	move $s5, $v0
	
	jal mirarMantizaPrimero
	move $s6, $v0
	
	jal mirarExponenteSegundo
	move $s7, $v0
	jal mirarMantizaSegundo
	move $s4, $v0

	j casosEspeciales

mirarExponentePrimero:
	beq $s2,0x7fff exponenteUno	
	beq $s2,0x0000 exponenteCero
	li $v0, 2
	jr $ra
	
mirarExponenteSegundo:
	beq $s3,0x7fff exponenteUno	
	beq $s3,0x0000 exponenteCero
	li $v0, 2
	jr $ra
	
mirarMantizaPrimero:
 #carga la primera parte de lamatntiza del primero
	lhu $t7,12($a0)
	addi $t7,$t7,0x00010000
	beq $t7,0x10000 mirarSegundaMantizaPrimero
	j mantizaUno	
	
mirarSegundaMantizaPrimero:
	lw  $t7 8($a0)	
	beq $t7,0x00000 mirarTerceraMantizaPrimero
	j mantizaUno		
	
mirarTerceraMantizaPrimero:
	lw  $t7 4($a0)	
	beq $t7,0x00000 mirarCuartaMantizaPrimero
	j mantizaUno		
	
mirarCuartaMantizaPrimero:
	lw  $t7 0($a0)	
	beq $t7,0x00000 mantizaCero		
	j mantizaUno

mirarMantizaSegundo:
 #carga la primera parte de lamatntiza del segundo	
	lhu $t7,12($a1)
	addi $t7,$t7,0x00010000
	move $v1, $t7
	move $t7 , $v1	
	beq $t7,0x10000 mirarSegundaMantizaSegundo
	j mantizaUno
	
mirarSegundaMantizaSegundo:
	lw  $t7 8($a1)	
	beq $t7,0x00000 mirarTerceraMantizaSegundo
	j mantizaUno		
	
mirarTerceraMantizaSegundo:
	lw  $t7 4($a1)	
	beq $t7,0x00000 mirarCuartaMantizaSegundo
	j mantizaUno		
	
mirarCuartaMantizaSegundo:
	lw  $t7 0($a1)	
	beq $t7,0x00000 mantizaCero		
	j mantizaUno
	
mantizaCero:
	move $v0,$zero 
	jr $ra 
	
mantizaUno:
	li $v0, 1
	jr $ra 
	
exponenteUno:
	li $v0, 1
	jr $ra 
exponenteCero:
	move $v0,$zero 
	jr $ra 
	
sumaNormal:	#####
	li $v0,4	#####
	syscall	#####
	
cargarSignos:
	lbu $t2,15($a0)
	lbu $t3,15($a1)
	srl $t5,$t2,7
	srl $t6,$t3,7
	move $v0 $t5
	move $v1 $t6
	jr $ra

cargarExponentes:
	lhu $t2,14($a0)
	lhu $t3,14($a1)
	li $t4 0x7fff
	and $t6 $t4 $t2
	move $v0 $t6
	and $t7 $t4 $t3	
	move $v1 $t7
	jr $ra
	
cargarMantiza:
	lhu $t7,12($a0)
	addi $t7,$t7,0x00010000
	move $v0, $t7
	lhu $t7,12($a1)
	addi $t7,$t7,0x00010000
	move $v1, $t7
	jr $ra

# Aqui comienza la caña y el sabor!!

casosEspeciales:

	move $t4, $s0#signo primero
	move $t0, $s5#expo primero
	move $t2, $s6#mantiza primero

	move $t5, $s1#signo segundo
	move $t1, $s7#expo segundo
	move $t3, $s4#mantiza segundo

############################################
#######                                                               ############
#######                  CREAR EL NUMERO                ############
#######                                                               ############
############################################
#   +0 = 0
#    -0 = 1
# +inf = 2
#  -inf = 3
#  NAN= 4
############################################


crearNumero1:
	beqz $t4, expo10 #si t0 = 0 e va  aanalizarExpo
	beqz $t0, analizarMantiza1 # 1,0,?
	
	beq $t0,2,numeroNormal1
	
	beqz $t2, menosInf1  # se va a hacer menosInf1 porque 1,1,0
	j NAN1   # 1,1,1
	
expo10: #Signo1 = 0
	beqz $t0, analizarMantiza11 #analizarmantiza.
	beqz $t2, masInf1 
	j NAN1      # 0,1,1
	
analizarMantiza1: # 1,0,?
	beqz $t2, menosCero1 # 1,0,0
	j numeroNormal1

analizarMantiza11: # 0,0,?
	beqz $t2, masCero1 # 0,0,0
	j numeroNormal1

NAN1:
	li $t8, 4 # hace NAN porque 1,1,1
	j crearNumero2
		
menosInf1:
	li $t8, 3
	j crearNumero2

masInf1:
	li $t8,2
	j crearNumero2

masCero1:
	li $t8, 0
	j crearNumero2

menosCero1:
	li $t8, 1
	j crearNumero2
	
numeroNormal1:
	li $t8, 5
	j crearNumero2

###############
### NUMERO DOS ###
###############

crearNumero2:
	beqz $t5, expo20 #si t0 = 0 e va  aanalizarExpo
	beqz $t1, analizarMantiza2 # 1,0,?
		
	beq $t1,2,numeroNormal2
	
	beqz $t3, menosInf2  # se va a hacer menosInf1 porque 1,1,0
	j NAN2    # 1,1,1
	
expo20: #Signo1 = 0
	beqz $t1, analizarMantiza22 #analizarmantiza.
	beqz $t3, masInf2 
	j NAN2      # 0,1,1
	
analizarMantiza2: # 1,0,?
	beqz $t3, menosCero2 # 1,0,0
	j numeroNormal2

analizarMantiza22: # 0,0,?
	beqz $t3, masCero2 # 0,0,0
	j numeroNormal2

NAN2:
	li $t9, 4 # hace NAN porque 1,1,1
	j sumaEspecial
		
menosInf2:
	li $t9, 3
	j sumaEspecial

masInf2:
	li $t9,2
	j sumaEspecial

masCero2:
	li $t9, 0
	j sumaEspecial

menosCero2:
	li $t9, 1
	j sumaEspecial
	
numeroNormal2:
	li $t9, 5
	j sumaEspecial

################
### UMA ESPECIAL ###
###############

sumaEspecial:
	
	beq $t8,4,esNAN1
	beq $t9,4,esNAN2
	
	beq $t8,0,primeroMasCero
	beq $t8,1,primeroMenosCero
	beq $t8,2,primeroMasInf
	beq $t8,3,primeroMenosInf
	beq $t8,5,primeroNormal
	
	
primeroMasCero:
	beq $t9,0, esMasCero
	beq $t9,1, esMenosCero
	beq $t9,2, esMasInf
	beq $t9,3, esMenosInf
	beq $t9,5, esNormal2 #aqui el segundo es el resul
	

primeroMenosCero:
	beq $t9,0,esMenosCero
	beq $t9,1,esMasCero
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,5,esNormal2 #aqui el segundo es el resul
	
primeroMasInf:
	beq $t9,0,esMasInf
	beq $t9,1,esMasInf
	beq $t9,2,esMasInf
	beq $t9,3,esNAN
	beq $t9,5,esMasInf
	
primeroMenosInf:
	beq $t9,0,esMenosInf
	beq $t9,1,esMenosInf
	beq $t9,3,esMenosInf
	beq $t9,2,esNAN
	beq $t9,5,esMenosInf

primeroNormal:
	beq $t9,0,esNormal1 # aqui el resutado es le primer numero
	beq $t9,1,esNormal1 # aqui el resutado es le primer numero
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,4,esNAN
	beq $t9,5,esNAN
	
esMasCero:
	li $t0 0x00000000
	sw $t0,0($a2)
	li $t0 0x00000000
	sw $t0,4($a2)
	li $t0 0x00000000
	sw $t0,8($a2)
	li $t0 0x00000000
	sw $t0,12($a2)
	j exit
	
esMenosCero:
	li $t0 0x00000000
	sw $t0,0($a2)
	li $t0 0x00000000
	sw $t0,4($a2)
	li $t0 0x00000000
	sw $t0,8($a2)
	li $t0 0x80000000
	sw $t0,12($a2)
	j exit

esMasInf:
	li $t0 0x00000000
	sw $t0,0($a2)
	li $t0 0x00000000
	sw $t0,4($a2)
	li $t0 0x00000000
	sw $t0,8($a2)
	li $t0 0x7fff0000
	sw $t0,12($a2)
	j exit
	
esMenosInf:
	li $t0 0x00000000
	sw $t0,0($a2)
	li $t0 0x00000000
	sw $t0,4($a2)
	li $t0 0x00000000
	sw $t0,8($a2)
	li $t0 0xffff0000
	sw $t0,12($a2)
	j exit

esNAN:
	li $t0 0x00000000
	sw $t0,0($a2)
	li $t0 0x00000000
	sw $t0,4($a2)
	li $t0 0x00000000
	sw $t0,8($a2)
	li $t0 0xffffffff
	sw $t0,12($a2)
	j exit
	

esNAN1:
	lw $t0,0($a0)
	sw $t0,0($a2)
	lw $t0,4($a0)
	sw $t0,4($a2)
	lw $t0,8($a0)
	sw $t0,8($a2)
	lw $t0,12($a0)
	sw $t0,12($a2)
	j exit
	
esNAN2:
	lw $t0,0($a1)
	sw $t0,0($a2)
	lw $t0,4($a1)
	sw $t0,4($a2)
	lw $t0,8($a1)
	sw $t0,8($a2)
	lw $t0,12($a1)
	sw $t0,12($a2)
	j exit

esNormal1:
	lw $t0,0($a0)
	sw $t0,0($a2)
	lw $t0,4($a0)
	sw $t0,4($a2)
	lw $t0,8($a0)
	sw $t0,8($a2)
	lw $t0,12($a0)
	sw $t0,12($a2)
	j exit

esNormal2:
	lw $t0,0($a1)
	sw $t0,0($a2)
	lw $t0,4($a1)
	sw $t0,4($a2)
	lw $t0,8($a1)
	sw $t0,8($a2)
	lw $t0,12($a1)
	sw $t0,12($a2)
	j exit
exit:
	lw $t0,  0($sp)
	lw $s0,32($sp)
	lw $s1,28($sp)
	lw $s2,24($sp)
	lw $s3,20($sp)
	lw $s4,16($sp)
	lw $s5,12($sp)
	lw $s6,8($sp)
	lw $s7,4($sp)
	lw $ra 0($sp)
	add $sp,$sp,36
	jr $t0

############################################
#######                                                               ############
#######                           BONUS                         ############
#######                                                               ############
############################################

bonus:

	add $sp,$sp,-36
	sw $s0,32($sp)
	sw $s1,28($sp)
	sw $s2,24($sp)
	sw $s3,20($sp)
	sw $s4,16($sp)
	sw $s5,12($sp)
	sw $s6,8($sp)
	sw $s7,4($sp)
	sw $ra 0($sp)
	
        jal cargarSignosB
	move $s0, $v0         #SINGO PRIMER NUMERO
	move $s1, $v1		#SIGNO SEGUNDO NUMERO
	jal cargarExponentesB
	move $s2, $v0         #EXPONENTE PRIMER NUMERO
	move $s3, $v1		#EXPONENTE SEGUNDO NUMERO
	 
	jal cargarMantizaB
	move $s4, $v0         #PRIMERA MANTIZA 1IMPLICITO
	move $s5, $v1		#SEGUNDA MANTIZA 1IMPLICITO
	 
	jal compararSignosB
	lw $t0,  0($sp)
	lw $s0,32($sp)
	lw $s1,28($sp)
	lw $s2,24($sp)
	lw $s3,20($sp)
	lw $s4,16($sp)
	lw $s5,12($sp)
	lw $s6,8($sp)
	lw $s7,4($sp)
	lw $ra 0($sp)
	add $sp,$sp,36
	jr $t0

cargarSignosB:
	lbu $t2,15($a0)
	lbu $t3,15($a1)
	srl $t5,$t2,7
	srl $t6,$t3,7
	move $v0 $t5
	move $v1 $t6
	jr $ra

cargarExponentesB:
	lhu $t2,14($a0)
	lhu $t3,14($a1)
	li $t4 0x7fff
	and $t6 $t4 $t2
	move $v0 $t6
	and $t7 $t4 $t3	
	move $v1 $t7
	jr $ra
	
cargarMantizaB:
	lhu $t7,12($a0)
	addi $t7,$t7,0x00010000
	move $v0, $t7
	lhu $t7,12($a1)
	addi $t7,$t7,0x00010000
	move $v1, $t7
	jr $ra

# Aqui comienza la caña y el sabor!!

compararSignosB:
	beq $s0,$s1 negativosB
	bgt $s1,$s0 primeroMayorB
	j segundoMayorB

compararExponenteB:
	beq $s2, $s3 compararMantisas1B 
	bgt $s2 , $s3 primeroMayorB
	j segundoMayorB

compararMantisas1B:
	beq $s4, $s5 compararMantisas2B
	bgt $s4, $s5 primeroMayorB
	j segundoMayorB

compararMantisas2B:
	lw  $s4 8($a0)
	lw  $s5 8($a1)
	beq $s4, $s5 compararMantisas3B
	bgt $s4, $s5 primeroMayorB
	j segundoMayorB

compararMantisas3B:
	lw  $s4 4($a0)
	lw  $s5 4($a1)
	beq $s4, $s5 compararMantisas4B
	bgt $s4, $s5 compararMantisas4B
	j segundoMayorB

compararMantisas4B:
	lw  $s4 0($a0)
	lw  $s5 0($a1)
	beq $s4, $s5 sonIgualesB
	bgt $s4, $s5 primeroMayorB
	j segundoMayorB

negativosB:
	beq  $s0,0 compararExponenteB
	bgt  $s2, $s3 	segundoMayorB	
	beq   $s2, $s3   mantizaNegativaB
	j primeroMayorB

mantizaNegativaB:
	beq $s4, $s5 mantizaNegativa1B
	bgt $s4, $s5  segundoMayorB
	j primeroMayorB

mantizaNegativa1B:
	lw  $s4 8($a0)
	lw  $s5 8($a1)
	beq $s4, $s5 mantizaNegativa2B
	bgt $s4, $s5 segundoMayorB
	j primeroMayorB

mantizaNegativa2B:
	lw  $s4 4($a0)
	lw  $s5 4($a1)
	beq $s4, $s5 mantizaNegativa3B
	bgt $s4, $s5 segundoMayorB
	j primeroMayorB

mantizaNegativa3B:
	lw  $s4 0($a0)
	lw  $s5 0($a1)
	beq $s4, $s5 sonIgualesB
	bgt $s4, $s5 segundoMayorB
	j primeroMayorB

primeroMayorB:
	li $t0 1
	move $t0,$v0
	jr $ra

segundoMayorB:
	li $t0 -1
	move $t0,$v0
	jr $ra

sonIgualesB:
	li $t0 0
	move $t0,$v0
	jr $ra

